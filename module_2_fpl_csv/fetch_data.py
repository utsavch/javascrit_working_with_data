import requests
import csv

def Fetch_Data(player_id):
    data = requests.get("https://fantasy.premierleague.com/drf/entry/" + player_id + "/history")
    return data.json()


def GetPlayerDetails(json_data):
    player_id = json_data['entry']['id']
    player_name = json_data['entry']['player_first_name']
    return (player_name, player_id)


def GetPlayerHistory(json_data):    
    return json_data['history'] # returns an array of the player's Gameweek Stats


def Write_CSV(player_history, player):
    csv_columns = list(player_history[0].keys()) # ['id', 'movement', 'points', 'total_points', 'rank', 'rank_sort', 'overall_rank', 'targets', 'event_transfers', 'event_transfers_cost', 'value', 'points_on_bench', 'bank', 'entry', 'event']
    csv_file = player[0] + " " + str(player[1]) + ".csv"
    try:
        with open(csv_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for player_data in player_history:
                writer.writerow(player_data)
    except IOError:
        print("I/O error")


if __name__ == "__main__":
    player_ids = ['3132852', '2547356', '712130', '935530' ]
    for pid in player_ids:
        json_data = Fetch_Data(pid)
        player_details = GetPlayerDetails(json_data)
        Write_CSV(GetPlayerHistory(json_data), player_details)